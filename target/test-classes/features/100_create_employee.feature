@smoke @create
Feature: Create Employee

  Scenario Outline: <testCase> <expectedResult>
    Given I want to create an employee with the following attributes:
      | firstName   | lastName   | email   |
      | <firstName> | <lastName> | <email> |

    And with the following phone numbers:
      | type   | isdCode | phoneNumber | extension |
      | Mobile | +31     | 686433636   |           |
      | Office | +31     | 886433636   | 333       |

    When I save the new employee '<testCase>'
    Then the save '<expectedResult>'

    Examples:
      | testCase                 | expectedResult | firstName | lastName | email                   |
      | WITHOUT FIRST NAME       | FAILS          |           | Kaya     | mahmutkaya.nl@gmail.com |
      | WITHOUT LAST NAME        | FAILS          | Mahmut    |          | mahmutkaya.nl@gmail.com |
      | WITHOUT EMAIL            | FAILS          | Mahmut    | Kaya     |                         |
      | WITH ALL REQUIRED FIELDS | IS SUCCESSFUL  | Mahmut    | Kaya     | mahmutkaya.nl@gmail.com |