@smoke @update
Feature: Update Employee

  Background:
    Given an employee with the following attributes:
      | firstName | lastName | email                   |
      | Mahmut    | Kaya     | mahmutkaya.nl@gmail.com |

    And with the following phone numbers:
      | type   | isdCode | phoneNumber | extension |
      | Mobile | +31     | 686433636   |           |
      | Office | +31     | 886433636   | 333       |

    When employee already exists


  Scenario Outline: <testCase> <expectedResult>
    Given I want to update an employee with the following attributes:
      | firstName   | lastName   | email   |
      | <firstName> | <lastName> | <email> |

    And with the following phone numbers:
      | type   | isdCode | phoneNumber | extension |
      | Mobile | +33     | 686433636   |           |
      | Office | +33     | 886433636   | 555       |

    When I save the employee by id '607c4373ee971419c4dc47ec' '<testCase>'
    Then the save '<expectedResult>'

    Examples:
      | testCase                 | expectedResult | firstName | lastName | email             |
      | WITHOUT FIRST NAME       | FAILS          |           | Doe      | johndoe@gmail.com |
      | WITHOUT LAST NAME        | FAILS          | John      |          | johndoe@gmail.com |
      | WITHOUT EMAIL            | FAILS          | John      | Doe      |                   |
      | WITH ALL REQUIRED FIELDS | IS SUCCESSFUL  | John      | Doe      | johndoe@gmail.com |
