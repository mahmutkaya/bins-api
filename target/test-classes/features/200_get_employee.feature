@smoke @get
Feature: Get Employee

  Background:
    Given an employee with the following attributes:
      | firstName | lastName | email                   |
      | Mahmut    | Kaya     | mahmutkaya.nl@gmail.com |

    And with the following phone numbers:
      | type   | isdCode | phoneNumber | extension |
      | Mobile | +31     | 686433636   |           |
      | Office | +31     | 886433636   | 333       |

    When employee already exists

  Scenario: GET BY ID
    When I want to get employee by id '607c4373ee971419c4dc47ec'
    Then the get 'IS SUCCESSFUL'
    And following employee is returned:
      | firstName | lastName | email                   |
      | Mahmut    | Kaya     | mahmutkaya.nl@gmail.com |

    And following employee phone numbers are returned:
      | type   | isdCode | phoneNumber | extension |
      | Mobile | +31     | 686433636   |           |
      | Office | +31     | 886433636   | 333       |

