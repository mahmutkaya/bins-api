bins-api
=================
- API Automation Testing Using Java, cucumber and restAssured

- api documentation: [jsonbin - bins api](https://jsonbin.io/api-reference/bins/get-started)

### dependencies:
- [cucumber-java](https://mvnrepository.com/artifact/io.cucumber/cucumber-java)
- [cucumber-junit]()
- [rest-assured](https://mvnrepository.com/artifact/io.rest-assured/rest-assured)
- [hamcrest](https://mvnrepository.com/artifact/org.hamcrest/hamcrest)

### Setting Up
These instructions will get you a copy of the project up and running on your local machine.

- *clone the repo:*
```shell
git clone https://gitlab.com/mahmutkaya/bins-api.git
```
- *create ```configuration.properties``` file at project level*
- *and add the text below into it with replacing your own values*
```properties
base_uri = https://api.jsonbin.io/v3/b
api_key = <API_KEY>
collection_id = <COLLECTION_ID>
```
