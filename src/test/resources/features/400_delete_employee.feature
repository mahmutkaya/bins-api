@smoke @delete
Feature: Delete Employee

  Background:
    Given an employee with the following attributes:
      | firstName | lastName | email                   |
      | Mahmut    | Kaya     | mahmutkaya.nl@gmail.com |

    And with the following phone numbers:
      | type   | isdCode | phoneNumber | extension |
      | Mobile | +31     | 686433636   |           |
      | Office | +31     | 886433636   | 333       |

    When employee already exists

  Scenario: DELETE BY ID
    When I want to delete employee by id '607ce744ee971419c4dca5ac'
    Then the delete 'IS SUCCESSFUL'
