package utilities;

import io.restassured.response.Response;

import java.util.Map;

import static io.restassured.RestAssured.given;

public class ApiUtils {

    private final static String   base_uri = ConfigReader.getProperty("base_uri");
    private final static String   api_key = ConfigReader.getProperty("api_key");
    private final static String   collection_id = ConfigReader.getProperty("collection_id");
    private static       Response response;

    public static Response post(String pathParams, Map<String, Object> reqBody) {
        pathParams = pathParams != null ? pathParams : "";
        try {
            response = given().headers(
                    "Content-Type", "application/json",
                    "X-Master-Key", api_key,
                    "X-Collection-Id", collection_id
            )
                              .body(reqBody)
                              .when()
                              .post(base_uri + pathParams);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

    public static Response get(String pathParams) {
        try {
            response = given().headers("X-Master-Key", api_key)
                              .when()
                              .get(base_uri + pathParams);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

    public static Response put(String pathParams, Map<String, Object> reqBody) {
        try {
            response = given().headers(
                    "Content-Type", "application/json",
                    "X-Master-Key", api_key
            )
                              .body(reqBody)
                              .when()
                              .put(base_uri + pathParams);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

    public static Response delete(String pathParams) {
        try {
            response = given().headers("X-Master-Key", api_key)
                              .when()
                              .delete(base_uri + pathParams);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

}
