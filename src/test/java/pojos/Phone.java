package pojos;

public class Phone {

    private String type;
    private String isdCode;
    private String phoneNumber;
    private String extension;

    public Phone() {
    }

    public Phone(
            String type, String isdCode, String phoneNumber, String extension
    ) {
        this.type        = type;
        this.isdCode     = isdCode;
        this.phoneNumber = phoneNumber;
        this.extension   = extension;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getIsdCode() {
        return isdCode;
    }

    public void setIsdCode(String isdCode) {
        this.isdCode = isdCode;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }
}
