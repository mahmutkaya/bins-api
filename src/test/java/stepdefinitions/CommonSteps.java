package stepdefinitions;

import io.restassured.response.Response;
import org.junit.Assert;

import static org.hamcrest.Matchers.anyOf;
import static org.hamcrest.Matchers.is;

public class CommonSteps {

    public static void handleExpectedResult(String expectedResult, Response response) {
        switch (expectedResult) {
            case "IS SUCCESSFUL":
                response.then().assertThat().statusCode(anyOf(is(200), is(201)));
                break;
            case "FAILS":
                //this code can be used if I knew more about making a field mandatory for api request
                //response.then().assertThat().statusCode(anyOf(is(400), is(401), is(403)));
                break;
            default:
                Assert.fail("Unexpected error");
        }
    }
}
