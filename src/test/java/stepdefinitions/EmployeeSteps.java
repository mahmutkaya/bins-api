package stepdefinitions;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.Response;
import org.junit.Assert;
import pojos.Employee;
import pojos.Phone;
import utilities.ApiUtils;

import java.util.*;

public class EmployeeSteps {

    Employee employee = new Employee();
    Phone    phone    = new Phone();

    private Response response;

    Map<String,Object> reqBody = new HashMap<>();

    @Given("(I want to create/update )an employee with the following attributes:")
    public void i_want_to_create_update_an_employee_with_the_following_attributes(List<Map<String, Object>> employeeDt) {
        Object firstName = employeeDt.get(0).get("firstName");
        Object lastName  = employeeDt.get(0).get("lastName");
        Object email     = employeeDt.get(0).get("email");

        if (firstName != null) employee.setFirstName(firstName.toString());
        if (lastName != null) employee.setLastName(lastName.toString());
        if (email != null) employee.setEmail(email.toString());

        //set request body
        reqBody.put("firstName", employee.getFirstName());
        reqBody.put("lastName", employee.getLastName());
        reqBody.put("email", employee.getEmail());
    }

    @Given("with the following phone numbers:")
    public void with_the_following_phone_numbers(DataTable phoneDt) {
        //convert DataTable to Java Object
        List<Phone> phoneList = phoneDt.asList(Phone.class);
        employee.setPhones(phoneList);

        //set request body
        reqBody.put("phones", employee.getPhones());
    }

    @When("I save the new employee {string}")
    public void I_save_the_new_employee(String string) {
        if(isRequiredFieldNull()) return;
        response = ApiUtils.post(null, reqBody);
    }

    @When("I save the employee by id {string} {string}")
    public void i_save_the_employee_by_id(String id, String expectedResult) {
        if(isRequiredFieldNull()) return;
        response = ApiUtils.put("/"+id, reqBody);
    }

    @When("employee already exists")
    public void employee_already_exists() {
        //implement this if you are able to get all bins of a collection
        //and get the desired bin's id with filtering by a custom id
        //but unfortunately get all bins endpoint is not working currently
    }

    @When("I want to get employee by id {string}")
    public void i_want_to_get_employee_by_id(String id) {
        response = ApiUtils.get("/"+id);
    }

    @Then("following employee is returned:")
    public void following_employee_is_returned(List<Map<String,Object>> expectedEmployeeLs) {
        //response body has record and metadata parent objects
        Map<String, Object> actualEmployee = response.jsonPath().get("record");
        Map<String, Object> expectedEmployee = expectedEmployeeLs.get(0);

        Assert.assertTrue(actualEmployee.entrySet()
                            .containsAll(expectedEmployee.entrySet()));
    }

    @Then("following employee phone numbers are returned:")
    public void following_employee_phone_numbers_are_returned(List<Map<String,Object>> expectedPhoneLs) {
        List<Map<String, Object>> actualPhoneLs = response.jsonPath().get("record.phones");

        Assert.assertEquals(expectedPhoneLs,actualPhoneLs);
    }

    @When("I want to delete employee by id {string}")
    public void i_want_to_delete_employee_by_id(String id) {
        response = ApiUtils.delete(id);
    }

    @Then("the save/get/delete {string}")
    public void the_save_get_delete(String expectedResult) {
        CommonSteps.handleExpectedResult(expectedResult, response);
    }

    private boolean isRequiredFieldNull(){
        //if a required field is null then exit without making post/put request
        return reqBody.get("firstName")==null ||
                reqBody.get("lastName")==null ||
                reqBody.get("email")==null;
    }
}
