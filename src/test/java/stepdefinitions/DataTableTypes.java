package stepdefinitions;

import io.cucumber.java.DataTableType;
import pojos.Phone;

import java.util.Map;

//convert DataTable to Java Object
public class DataTableTypes {

    @DataTableType
    public Phone phoneEntry(Map<String, String> entry) {
        return new Phone(
                entry.get("type"),
                entry.get("isdCode"),
                entry.get("phoneNumber"),
                entry.get("extension")
        );
    }
}
